import pickle

__author__ = 'Andriy Drozdyuk'


class A(object):
    def __init__(self, msg):
        self.msg = msg


a = A("Hello world!")

serialized = pickle.dumps(a)
b = pickle.loads(serialized)
assert (b.msg == 'Hello world!')
